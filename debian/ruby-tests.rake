require 'gem2deb/rake/spectask'

desc 'Default: run the specs and features.'
task default: %w(spec:unit spec:acceptance features)

namespace :spec do
  desc "Run unit specs"
  Gem2Deb::Rake::RSpecTask.new('unit') do |t| 
    t.pattern = 'spec/{*_spec.rb,factory_bot/**/*_spec.rb}'
  end 

  desc "Run acceptance specs"
  Gem2Deb::Rake::RSpecTask.new('acceptance') do |t| 
    t.pattern = 'spec/acceptance/**/*_spec.rb'
  end 
end
